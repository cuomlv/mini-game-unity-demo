import axios from 'axios'
import { useRuntimeConfig } from '~/.nuxt/imports'

const http = axios.create({
  baseURL: 'http://192.168.12.57:3333/api',
})

http.interceptors.request.use(
  function (config) {
    config.headers.Authorization = `Bearer oat_Mjg.bzRISHQ1SFRvZHJLLXFVNk9DaTRmSTNxRjhUbVlTSm53amRJbXFxTDE0NzM4MTk2NzI`
    config.headers.Accept = 'application/json'
    return config
  },
  function (error) {
    return Promise.reject(error)
  },
)

http.interceptors.response.use(
  function (response) {
    return response
  },
  async function (error) {
    throw error
  },
)

export default http
