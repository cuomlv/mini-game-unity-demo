// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: '2024-04-03',
  devtools: {
    enabled: true
  },
  app: {
    head: {
      title: 'Mini game demo',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      htmlAttrs: { lang: 'en' },
      meta: [{ name: 'description', content: 'Mini game demo' }],
      script: [{ src: 'https://telegram.org/js/telegram-web-app.js' }],
    },
  },

  modules: [
    [
      '@pinia/nuxt',
      {
        autoImports: ['defineStore', ['defineStore', 'definePiniaStore'], 'acceptHMRUpdate'],
      },
    ],

    // Tailwind & Nuxt 2/3: https://tailwindcss.com/docs/guides/nuxtjs
    '@nuxtjs/tailwindcss',

    // Ant Design: https://antdv.com/components/overview
    '@ant-design-vue/nuxt',
  ],

  imports: {
    dirs: ['stores'],
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  alias: {
    'tiny-case': 'tiny-case/index.js',
    'property-expr': 'property-expr/index.js',
    toposort: 'toposort/index.js',
  },

  runtimeConfig: {
    public: {
      environment: process.env,
    },
  },

  devServer: {
    port: 4000,
  },

})
