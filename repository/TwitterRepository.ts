import { AxiosResponse } from 'axios'
import BaseRepository from '~/repository/BaseRepository'

export default class TwitterRepository extends BaseRepository {
  static readonly BASE_RESOURCE: string = '/user/twitter'

  public async redirect(): Promise<AxiosResponse> {
    this.setResource(`${TwitterRepository.BASE_RESOURCE}/redirect`)

    return await this.get()
  }

  public async authorize(params): Promise<AxiosResponse> {
    this.setResource(`${TwitterRepository.BASE_RESOURCE}/authorize`)

    return await this.get({}, params)
  }
}