import { AxiosResponse } from 'axios'
import BaseRepository from '~/repository/BaseRepository'

export default class TelegramRepository extends BaseRepository {
  static readonly BASE_RESOURCE: string = '/user/telegram'

  public async authorize(payload): Promise<AxiosResponse> {
    this.setResource(`${TelegramRepository.BASE_RESOURCE}/authorize`)

    return await this.post(payload)
  }
}