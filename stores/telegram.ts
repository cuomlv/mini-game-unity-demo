import { defineStore } from 'pinia'
import TelegramRepository from '~/repository/TelegramRepository'

const telegramRepository = new TelegramRepository()

export const useTelegramStore = defineStore('telegram', {
  state: () => ({
    authToken: null,
  }),

  actions: {
    async login(payload) {
      try {
        const { data } = await telegramRepository.authorize(payload)
        this.authToken = data
      } catch (error) {
        console.log(error)
      } finally {
      }
    },
  },
})