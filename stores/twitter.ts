import { defineStore } from 'pinia'
import TwitterRepository from '~/repository/TwitterRepository'

const twitterRepository = new TwitterRepository()

export const useTwitterStore = defineStore('twitter', {
  state: () => ({
    authToken: null,
  }),

  actions: {
    async redirect() {
      try {
        const { data } = await twitterRepository.redirect()

        window.location.href = data.authUrl
      } catch (error) {
        console.log(error)
      } finally {
      }
    },

    async authorize(params) {
      try {
        const { data } = await twitterRepository.authorize(params)

        console.log(data)
      } catch (error) {
        console.log(error)
      } finally {
      }
    },
  },
})